﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        : BaseEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }
        [JsonIgnore]
        public virtual ICollection<Customer> Customers { get; set; }
    }
}